
//this test cases asumes nothing 
//at the end there no change
import  {logInToCatoUsingRequest, LogInToCatoManually} from '../../../support/login.js'
import {crossIconForResolutionDialog,projecrtsDescribtion,projectsName,customApp,checkBoxForAllCustomApp,deleteIcon,okInDeleteDialog,addIcon,nameField,describtionField,okInAddDialog} from '../../../support/customApp.js'


describe('delete',function(){

    
    before(() => {
        cy.logInToCatoUsingRequest()
        cy.MaximizeWindwoSize()
        cy.get(crossIconForResolutionDialog).click({force:true})//in case the resolution dialog pop up
        
    })
    

    it('Delete a custom application ', () => {
        //add custome application with name 'to delete'
        cy.get(addIcon).click()
        cy.get(nameField).type('to delete')
        cy.get(describtionField).type('these app added to test the delete functionality')
        cy.get(okInAddDialog).click()

        cy.server()
        cy.route('POST','getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
        // wait for GET getCustomAppDefinitions?customapplicationId=.*
        cy.wait('@addedSuccessfully').its('status').should('eq', 200)

        //delete 'to delete' app
        cy.get(customApp).contains('to delete').click()
        cy.get(deleteIcon).click()
        cy.get(okInDeleteDialog).click()

        cy.server()
        cy.route('POST','getAllCustomApps?accountId=*').as('deletedSuccessfully')
        cy.wait('@deletedSuccessfully').its('status').should('eq', 200)

        //verify that the custom application with name ‘to delete’ is not exist
        cy.get(customApp).should('not.contain.text','to delete')

    })


    

    

})