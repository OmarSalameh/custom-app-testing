
//this test ceses assume nothing
//at the end a new custom app will added ,  name and desicription for the the app will changed 

import  {logInToCatoUsingRequest, LogInToCatoManually} from '../../../support/login.js'
import {crossIconForResolutionDialog,save,describtionInGeneral,nameInGeneral,general,projecrtsDescribtion,projectsName,customApp,checkBoxForAllCustomApp,deleteIcon,okInDeleteDialog,addIcon,nameField,describtionField,okInAddDialog} from '../../../support/customApp.js'


describe('custome application', function () {

    beforeEach(() => {
        logInToCatoUsingRequest()
        cy.MaximizeWindwoSize()
        cy.get(crossIconForResolutionDialog).click({force:true})//in case the resolution dialog pop up
    })




    it('Edit the name and description of a custom application ', () => {
        
        
        cy.fixture('edit.json').then((edit)  => {
          

          
      
        //add custom app :
        cy.get(addIcon).click()
        cy.get(nameField).type('test edit functionality')
        cy.get(describtionField).type('description')
        cy.get('#addAppBtn').click()

        cy.server()
        cy.route('POST', 'getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
        // wait for GET getCustomAppDefinitions?customapplicationId=.*
        cy.wait('@addedSuccessfully').its('status').should('eq', 200)

        //edit name and description:
        cy.get(customApp).first().click()
        cy.get(general).click()
        cy.get(nameInGeneral).clear().type(edit.newName)
        cy.get(describtionInGeneral).clear().type(edit.newDescription)
        cy.get(save).click()



        cy.server()
        cy.route('POST', 'getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
        // wait for GET getCustomAppDefinitions?customapplicationId=.*
        cy.wait('@addedSuccessfully').its('status').should('eq', 200)

        //check all projects name and assert if there is a project with name 'new project name'
        cy.get(projectsName).should('contain.text', edit.newName)

        //check all projects description and assert if there is a project with description 'new project description'
        cy.get(projecrtsDescribtion).should('contain.text', edit.newDescription)



    })

            
})

})