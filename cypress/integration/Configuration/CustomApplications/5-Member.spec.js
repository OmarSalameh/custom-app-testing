
//this suit assume to have one custom app and that app has no member 

import { logInToCatoUsingRequest, LogInToCatoManually } from '../../../support/login.js'
import {crossIconForResolutionDialog,crossIconInAddCategoryDialog,cancelButtonInAddCategoryDialog,checkBoxForAbortion,allCheckBoxInCategoryDialog, crossIconForCategory, addedCategory, okInCategoryDialog, Categoey, addOrEditCategory, save, describtionInGeneral, nameInGeneral, general, projecrtsDescribtion, projectsName, customApp, checkBoxForAllCustomApp, deleteIcon, okInDeleteDialog, addIcon, nameField, describtionField, okInAddDialog } from '../../../support/customApp.js'


describe('add memmber', function () {

    beforeEach(() => {
        logInToCatoUsingRequest()
        cy.MaximizeWindwoSize()
        cy.get(crossIconForResolutionDialog).click({force:true})//in case the resolution dialog pop up

    })


    it('Add one Categories to the custom application', function () {
        cy.fixture('addCustom.json').then((NewApp) => {
            cy.get(customApp).eq(0).click()
            cy.get(general).click()
            //Verify the icon that add member contain text ‘add’, when there is no member for the application 
            cy.get(addOrEditCategory).should('contain.text', 'Add')
            cy.get(addOrEditCategory).click()
            cy.get(Categoey(NewApp.Category)).click({ force: true })
            cy.get(okInCategoryDialog).click()
            cy.get(save).click()

            //verify that the add icon changed to edit.
            cy.get(addOrEditCategory).should('contain.text', 'Edit')
            //verify that the abortion is added
            cy.get(addedCategory).should('contain.text', NewApp.Category)

        })
    })


    it('delete abortion  member using x icon', function () {
        cy.fixture('addCustom.json').then((NewApp) => {
            cy.get(customApp).eq(0).click()
            cy.get(general).click()


            cy.get(addedCategory)
                .contains(NewApp.Category)
                .parent()
                .parent().within(function () {
                    cy.get(crossIconForCategory).click()
                })

            cy.get(save).click()
            cy.get(addedCategory).should('have.length', 0)
        })
    })

    it('Delete abortion categories using edit icon.', function () {
        cy.fixture('addCustom.json').then((NewApp) => {
            // add abortion member 
            cy.get(customApp).eq(0).click()
            cy.get(general).click()
            cy.get(addOrEditCategory).click()
            cy.get(Categoey(NewApp.Category)).click({ force: true })
            cy.get(okInCategoryDialog).click()
            cy.get(save).click()


            //delete member abortion
            cy.get(addOrEditCategory).click()
            cy.get(Categoey(NewApp.Category)).click({ force: true })
            cy.get(okInCategoryDialog).click()
            cy.get(save).click()
            cy.get(addedCategory).should('not.contain.text', NewApp.Category)



        })
    })

    it('Delete all member by using clear', function () {
        //check all the member
        cy.get(customApp).eq(0).click()
        cy.get(general).click()
        cy.get(addOrEditCategory).click()
        cy.get(allCheckBoxInCategoryDialog).each(function (element) {
            element.click()
        })
        cy.get(okInCategoryDialog).click()//click ok
        cy.get(save).click()//click save

        //use clear
        cy.get(addOrEditCategory).click()//click add
        cy.contains('Clear').click({ force: true })
        cy.get(okInCategoryDialog).click()//click ok
        cy.get(save).click()//click save
        cy.get(addedCategory).should('have.length', 0)


    })

    it('Cancel the operation of add categories using cancel', function () {
        cy.get(customApp).eq(0).click()
        cy.get(general).click()
        cy.get(addOrEditCategory).click()
        cy.get(checkBoxForAbortion).click({ force: true })
        cy.get(cancelButtonInAddCategoryDialog).click()
        cy.get(save).should('be.disabled')

    })

    it('Cancel the operation of add categories using x icon', function () {

        cy.get(customApp).eq(0).click()
        cy.get(general).click()
        cy.get(addOrEditCategory).click()
        cy.get(checkBoxForAbortion).click({ force: true })
        cy.get(crossIconInAddCategoryDialog).click()
        cy.get(save).should('be.disabled')


    })




})