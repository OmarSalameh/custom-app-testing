
//this suit assume nothing
/// <reference types="cypress" />
import  {logInToCatoUsingRequest, LogInToCatoManually} from '../../../support/login.js'
import {crossIconForResolutionDialog,projecrtsDescribtion,projectsName,customApp,checkBoxForAllCustomApp,deleteIcon,okInDeleteDialog,addIcon,nameField,describtionField,okInAddDialog, Categoey} from '../../../support/customApp.js'

describe('negative test on add ',function(){

    beforeEach(() => {
        logInToCatoUsingRequest()
        cy.MaximizeWindwoSize()
        cy.get(crossIconForResolutionDialog).click({force:true})//in case the resolution dialog pop up
    })

    

it('add tow project with same name', function () {

    // add first custom app whith name : 'name1'
    cy.get(addIcon).click()
    cy.get(nameField).type('name1')
    cy.get(describtionField).type('add two custom app whith sam name')
    cy.get(okInAddDialog).click()

    cy.server()
    cy.route('POST', 'getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
    // wait for GET getCustomAppDefinitions?customapplicationId=.*
    cy.wait('@addedSuccessfully').its('status').should('eq', 200)


  // add second custom app whith name : 'name1'
    cy.get(addIcon).click()
    cy.get(nameField).type('name1')
    cy.get(describtionField).type('add two custom app whith sam name')
    cy.get(okInAddDialog).click()

    cy.server()
    cy.route('POST', 'getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
    // wait for GET getCustomAppDefinitions?customapplicationId=.*
    cy.wait('@addedSuccessfully').its('status').should('eq', 200)

let count =0
    cy.get(customApp).each(function($element){
       
        if(Element.text == 'name1'){
            count++
        }
        
    })

    if(count==1){
        cy.log('assertion pass, there is 1 custom app with name: "name1"')
        //assrtion pass,the following assertion is always pass
      cy.get(customApp).contains('name1').should("have.length",1)
      
    }else{
        cy.log('assertion fall, there is more than 1 custom app with name: "name1"')
        //assetion fall,the following assertion is always fall
        cy.get(customApp).contains('name1').should("have.length",0)
    }
      
    



})

it('Add custom app with name contain special character.', () => {
    cy.get(addIcon).click()
    cy.get(nameField).type('@_-!#$1')
    cy.get(describtionField).type('description')
    cy.get(okInAddDialog).click()

    cy.server()
    cy.route('POST', 'getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
    // wait for GET getCustomAppDefinitions?customapplicationId=.*
    cy.wait('@addedSuccessfully').its('status').should('eq', 200)

    cy.get(customApp).should('not.contain.text', '@_-!#$1')


})



it('Add custom app with name contain white space. ', () => {
    cy.get(addIcon).click()
    cy.get(nameField).type('my project with white space')
    cy.get(describtionField).type('description')
    cy.get(okInAddDialog).click()

    cy.server()
    cy.route('POST', 'getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
    // wait for GET getCustomAppDefinitions?customapplicationId=.*
    cy.wait('@addedSuccessfully').its('status').should('eq', 200)

    cy.get(customApp).should('contain.text', 'my project with white space')


})



it('add custom app with description contain 500 characters', () => {
    cy.get(addIcon).click()
    cy.get(nameField).type('app with description contain 500 characters')
     

    let a
    for (let i = 0; i < 50; i++) {
        a += 'hellow cy!'
    }
    cy.get(describtionField).type(a)
    cy.get(okInAddDialog).click()

    cy.server()
    cy.route('POST', 'getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
    // wait for GET getCustomAppDefinitions?customapplicationId=.*
    cy.wait('@addedSuccessfully').its('status').should('eq', 200)

    cy.get(customApp).should('contain.text', a)


})

it('add custom app with description contain 1000 characters', () => {
    cy.get(addIcon).click()
    cy.get(nameField).type('app with description contain 1000 characters')
 


    let a
    for (let i = 0; i < 100; i++) {
        a += 'hellow cy!'
    }
    cy.get(describtionField).type(a)
    cy.get('#addAppBtn').click()

    cy.server()
    cy.route('POST', 'getCustomAppDefinitions?customapplicationId=*').as('addedSuccessfully')
    // wait for GET getCustomAppDefinitions?customapplicationId=.*
    cy.wait('@addedSuccessfully').its('status').should('eq', 200)

    cy.get(customApp).should('contain.text', a)


})




})