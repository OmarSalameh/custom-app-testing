/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
/*

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
}
*/
/*
module.exports = (on, config) => {
  // the rest of your plugins... 
  require('cypress-log-to-output').install(on)
  // or, if there is already a before:browser:launch handler, use .browserLaunchHandler inside of it
  // @see https://github.com/flotwig/cypress-log-to-output/issues/5
}
*/

module.exports = (on, config) => {
  const options = {};

  if (config.env.generateOutput == "1") {
    options.outputRoot = config.projectRoot + '/output/';
    options.outputTarget = {
      'not/existing/path/out.txt': 'txt',
      'out.txt': 'txt',
      'out.json': 'json',
      'out.cst': function (messages) {
        let data = Object.keys(messages).join('\n') + '\n';
        this.writeChunk(data);
      },
    };
  }

  if (config.env.compactLogs == "1") {
    options.compactLogs = 1;
  }

  require('cypress-terminal-report/src/installLogsPrinter')(on,options);
};
