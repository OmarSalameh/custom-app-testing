/// <reference types="cypress" />

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('LogInToCatoManually', () => {

    cy.visit("/")
    cy.get('#email').type('emad.aldalu+100@exalt.ps')
    cy.get('#passwordfield').type('exalt12345')
    cy.get('#submit').click()

})


Cypress.Commands.add('logInToCatoUsingRequest', () => {

    cy.request({
        method: 'POST',
        url: 'https://test.catonet.works/ticker/handlelogin',
        // qs : pass parameter to the url
        body: {
            'email': 'emad.aldalu+100@exalt.ps',
            'pw': 'exalt12345'

        },
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'ar-AE,ar;q=0.9,en-US;q=0.8,en;q=0.7',
            'Connection': 'keep-alive',
            'Content-Length': 48,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': 'auth-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlbWFkLmFsZGFsdSsxMDBAZXhhbHQucHMiLCJsYXN0TmFtZSI6IkFsZGFsdSIsInZlciI6MSwicm9sZSI6IkVESVRPUiIsIm5pY2tOYW1lIjoiRW1hZCBBbGRhbHUiLCJhY2NvdW50VHlwZSI6IlJFR1VMQVIiLCJpc3MiOiJodHRwczovL2NjMi5jYXRvbmV0d29ya3MuY29tIiwiYWNjb3VudFBhdGgiOiJfMV8xNjU2XyIsImlzUm9sZUVsZXZhdGVkQnlVc2VyIjpmYWxzZSwiaWRudCI6MTIzMjgsImZpcnN0TmFtZSI6IkVtYWQiLCJhcHBsaWVkUm9sZSI6IkVESVRPUiIsImlhdCI6MTU5MzUwMTY2MiwiYWNjb3VudFRPVUFwcHJvdmVkIjp0cnVlfQ.AW7G0rTJvcQEHbEtaEk_Pn7OreUixB2MMjfytS7fVRo',
            'Host': 'test.catonet.works',
            'Origin': 'https://test.catonet.works',
            'Referer': 'https://test.catonet.works/login',
            'Sec-Fetch-Dest': 'empty',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Site': 'same-origin',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36'

        }
    }).its('headers')
        .then(function (e) {

            let cookie
            let cookieContent
            for (let i = 2; i < e["set-cookie"].length; i++) {
                cookie = e["set-cookie"][i].split(";")
                for (let j = 0; j < cookie.length; j++) {
                    cookieContent = cookie[j].split('=')
                    if (cookieContent.length == 2 && cookieContent[0] == 'auth-token') {
                        cy.setCookie(cookieContent[0], cookieContent[1])
                    }
                        
                    
                }
            }

        })

    cy.visit('/#!/1656/customApplications')




})

Cypress.Commands.add("MaximizeWindwoSize",function(){

    module.exports = (on, config) => {
        on('before:browser:launch', (browser = {}, launchOptions) => {
          if (browser.family === 'chromium' && browser.name !== 'electron') {
            launchOptions.args.push('--start-fullscreen')
      
            return launchOptions
          }
      
          if (browser.name === 'electron') {
            launchOptions.preferences.fullscreen = true
      
            return launchOptions
          }
        })
      }
})

