/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable<Subject> {
        /**
         * log in to cato websit manually 
         */
        LogInToCatoManually(): void
        /**
         * log in to cato websit by sending requist and set Cookie
         */
        logInToCatoUsingRequest():void// to specify the argument replace this line with:logInToCatoUsingRequest(URL : string): Chainable<any>
        /**
         * maximize window size
         */
        MaximizeWindwoSize():void
    }
}
